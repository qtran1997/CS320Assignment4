Quang Tran
qtran1997@gmail.com
CS320 Assignment 4
819258733

Project Description: This project contains 3 python files. The first file is a standalone program while the other two are used together.

File 1: prog4_1.py: This program takes in a text file and tokenizes each word using spaces. It adds a comma based after every word and is separated based on the line in the text file.

File 2: StackMachine.py: This program is a stack. It can perform functions: push, pop, add, subtract, multiply, divide, and mod. However, the last 6 functions depend on the items in the stack.

File 3: prog4_2.py: This program is very similar to the first file, however, the tokenization of each word is important. This file is linked to StackMachine.py and if the tokenization is the same as the name of the functions, the function is called in StackMachine.py
