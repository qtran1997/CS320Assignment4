import sys
import StackMachine

def main():
    count = 0      
    stack = StackMachine.StackMachine()
    print("Assignment #4-2, Quang Tran, qtran1997@gmail.com")
    with open(sys.argv[1],'r') as f:
        for line in f:           
            for word in line.split():                           
                if(count == 0 and  (word == "push")):  
                    count = 1 
                elif(count == 1 and word.isdigit()):
                    stack.push(word)                
                    count = 0
                elif(word == "pop"):
                    print(stack.pop())                           
                    count = 0
                elif(word == "add"):    
                    stack.add()                 
                    count = 0
                elif(word == "sub"):  
                    stack.sub()                 
                    count = 0
                elif(word == "mul"):    
                    stack.mul()                 
                    count = 0
                elif(word == "div"):  
                    stack.div()                 
                    count = 0
                elif(word == "mod"):    
                    stack.mod()                 
                    count = 0
                else:
                    count = 0

