import sys
class StackMachine(object):
    def __init__(self):
        self.stack = []

    def push(self,tmp):
        self.stack.append(tmp)

    def pop(self):
        if(len(self.stack) != 0):
            return self.stack.pop()  
        else:
            return None  

    def add(self):
        if(len(self.stack) > 1):
            tmp1 = self.stack.pop()
            tmp2 = self.stack.pop()
            StackMachine.push(self,int(tmp1)+int(tmp2))
    def sub(self):
        if(len(self.stack) > 1):
            tmp1 = self.stack.pop()
            tmp2 = self.stack.pop()
            StackMachine.push(self,int(tmp1)-int(tmp2))
    def mul(self):
        if(len(self.stack) > 1):
            tmp1 = self.stack.pop()
            tmp2 = self.stack.pop()
            StackMachine.push(self,int(tmp1)*int(tmp2))
    def div(self):
        if(len(self.stack) > 1):
            tmp1 = self.stack.pop()
            tmp2 = self.stack.pop()
            StackMachine.push(self,int(tmp1)/int(tmp2))
    def mod(self):
        if(len(self.stack) > 1):
            tmp1 = self.stack.pop()
            tmp2 = self.stack.pop()
            StackMachine.push(self,int(tmp1)%int(tmp2))

